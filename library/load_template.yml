---
 - name: Define cookbook templates
   tower_job_template:
     name: "{{ mytemplate.name }}"
     description: "{{ mytemplate.description }}"
     job_type: run
     job_tags: "{{ mytemplate.job_tags | default('') }}"
     skip_tags: "{{ mytemplate.job_skip_tags | default('') }}"
     inventory: "{{ mytemplate.inventory }}"
     project: "{{ mytemplate.project }}"
     playbook: "{{ mytemplate.playbook }}"
     credential: "{{ mytemplate.job_machine_cred }}"
     vault_credential: "{{ mytemplate.job_vault_cred }}"
     fact_caching_enabled: yes
     state: present
     tower_host: "{{ tower_url }}"
     tower_username: "{{ vault_awx_admin_user }}"
     tower_password: "{{ vault_awx_admin_password }}"
   loop: "{{ awx_templates }}"
   loop_control:
     loop_var: mytemplate
   delegate_to: localhost
   tags: load_templates

 - name: Modify the virtual-env for the templates
   shell: "tower-cli job_template modify \
     -h {{ tower_url }} -u {{ vault_awx_admin_user }} -p {{ vault_awx_admin_password }} \
     --name \"{{ mytemplate.name }}\" --project \"{{ mytemplate.project }}\" \
     --playbook \"{{ mytemplate.playbook }}\" \
     --custom-virtualenv \"/var/lib/awx/venv/ansible-{{ mytemplate.ansible_version | default('awx_default_ansible_version') }}\""
   register: towerstatus
   when: mytemplate.ansible_version is defined and mytemplate.ansible_version in awx_ansible_versions
   loop: "{{ awx_templates }}"
   loop_control:
     loop_var: mytemplate
   delegate_to: localhost
   tags: load_templates

 - name: Set extra-vars for the templates
   shell: "tower-cli job_template modify \
     -h {{ tower_url }} -u {{ vault_awx_admin_user }} -p {{ vault_awx_admin_password }} \
     --name \"{{ mytemplate.name }}\" --project \"{{ mytemplate.project }}\" \
     --playbook \"{{ mytemplate.playbook }}\" \
     --extra-vars \"{{ mytemplate.extra_vars }}\""
   register: towerstatus
   when: mytemplate.extra_vars is defined
   loop: "{{ awx_templates }}"
   loop_control:
     loop_var: mytemplate
   delegate_to: localhost
   tags: load_templates

 - name: Setup Slack notification for these templates
   shell: "tower-cli job_template associate_notification_template \
     -h {{ tower_url }} -u {{ vault_awx_admin_user }} -p {{ vault_awx_admin_password }} \
     --job-template \"{{ mytemplate.name }}\" \
     --notification-template \"Tower Slack\" --status error"
   register: towerstatus
   loop: "{{ awx_templates }}"
   loop_control:
     loop_var: mytemplate
   delegate_to: localhost
   tags: load_templates

 - name: Grant the correct permissions on the templates
   shell: "tower-cli role grant
     -h {{ tower_url }} -u {{ vault_awx_admin_user }} -p {{ vault_awx_admin_password }} \
     --team \"{{ mytemplateacl.team }}\" --type \"{{ mytemplateacl.permission }}\" --job-template \"{{ mytemplateacl.name }}\""
   register: towerstatus
   loop: "{{ awx_template_acl }}"
   loop_control:
     loop_var: mytemplateacl
   delegate_to: localhost
   tags: load_templates

 - name: Create the correct schedule for these templates
   shell: "tower-cli schedule create \
     -h {{ tower_url }} -u {{ vault_awx_admin_user }} -p {{ vault_awx_admin_password }} \
     -n \"{{ mytemplate.name }}\" \
     -d \"Schedule for {{ mytemplate.name }}\"
     --job-template \"{{ mytemplate.name }}\" --enabled false \
     --rrule \"DTSTART:{{ lookup('pipe','date +%Y%m%d') }}T{{ mytemplate.schedule_time | default('004500') }}Z RRULE:FREQ={{ mytemplate.schedule_freq | default('HOURLY') }};INTERVAL={{ mytemplate.schedule_interval | default('1') }}\""
   register: towerstatus
   when: mytemplate.schedule
   loop: "{{ awx_templates }}"
   loop_control:
     loop_var: mytemplate
   delegate_to: localhost
   tags: load_templates
